class game{
  constructor(){
    if(this.constructor === game){
      throw new Error("This is Abstract Class");
    }
  }

checkresult(PlayerChoice,ComChoice){
  if((PlayerChoice ==="rock" && ComChoice==="scissors") 
 || (PlayerChoice ==="scissors" && ComChoice==="paper") 
 || (PlayerChoice ==="paper" && ComChoice==="rock")){
    return "You win!!"
    } else if((PlayerChoice ==="rock" && ComChoice==="paper") || 
    (PlayerChoice ==="scissors" && ComChoice==="rock") ||
     (PlayerChoice ==="paper" && ComChoice==="scissors")){
         return "You Lose!!"
    } else if(PlayerChoice === ComChoice){
        return "Draw!!"
    }
}

static comchoice(){
  let choice = ["rock","paper","scissors"];
  let random = Math.floor(Math.random()*3);
  return choice[random]
}

}

class Suit extends game{
  constructor(tag,func){
    super()
    this.tag = tag
    this.func = func
  
  }

  choosen(){
  this.tag.addEventListener("click", this.func)    
  }
}

class ResultSuit extends game{
  constructor(tag){
    super()
    this.tag = tag
  }

  showresult(hasil){
    this.tag.innerHTML = hasil
    this.tag.classList.add("result") 
  
  }
}

let started = true

const suit = document.querySelectorAll(".suit")
const resultsuit =  new ResultSuit(document.querySelector("#versus"))
const resetbtn = document.querySelector(".refresh")


const batu = new Suit(suit[0],pilih)
const gunting = new Suit(suit[1],pilih)
const kertas = new Suit(suit[2],pilih)
const resetgame = new Suit(resetbtn,reset)


function pilih(suit){
  let _tag = suit.target

  if(started){
    _tag.classList.add("choice")
    let com = game.comchoice()
    let hasil = resultsuit.checkresult(_tag.name , com)
    console.log("Com : "+com)
    console.log("Player : "+_tag.name)
    resultsuit.showresult(hasil)
    document.querySelector("#"+com+"com").classList.add("choice")
    console.log(hasil)
    started = false
  } 
}


batu.choosen()
gunting.choosen()
kertas.choosen()
resetgame.choosen()


function reset(){
  const vs = document.querySelector("#versus")
  ComChoice ="";
  PlayerChoice = "";
  vs.innerHTML = "VS";
  vs.classList.remove("result");
  removeclass();
  started = true;
}

function removeclass(){
  const hapus = document.querySelectorAll("img");
  for(let a=0; a<hapus.length ;a++){
    hapus[a].classList.remove("choice");
}
}